<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PanelSession extends Model
{
    /**
     * Get the panel that owns the session.
     */
    public function panel()
    {
        return $this->belongsTo('App\Panel', 'panel_id');
    }
}
