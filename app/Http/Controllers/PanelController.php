<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Panel;
use App\User;

class PanelController extends Controller
{
    public function index()
    {
        $user = User::find(1);
        $panels = $user->panels()->orderby('created_at', 'desc')->get();
        //$panels = Panel::orderby('created_at', 'desc')->get();
        return view('panels.index', compact('panels'));
    }

    // Route-model binding
    public function show(Panel $panel)
    {
        return view('panels.show', compact('panel'));
    }
}
