<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Panel extends Model
{
    /**
     * The users that can access this panel to see results and change settings.
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'permissions');
    }

    /**
     * Get the sessions for the panel.
     */
    public function panelSessions()
    {
        return $this->hasMany('App\PanelSession', 'panel_id');
    }
}
