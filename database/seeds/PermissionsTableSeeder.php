<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            'user_id' => 1,
            'panel_id' => 1,

            // Seed timestamps
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
