<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PanelSessionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('panel_sessions')->insert([
            'panel_id' => 1,
            'session_number' => 1,
            'key' => '09AJIUA8',

            // Seed timestamps
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
