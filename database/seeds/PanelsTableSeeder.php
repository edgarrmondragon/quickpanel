<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PanelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('panels')->insert([
            'name' => 'Fine Fragrances for Men',

            // Seed timestamps
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('panels')->insert([
            'name' => 'Napping',

            // Seed timestamps
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
