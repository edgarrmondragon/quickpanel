<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePanelSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('panel_sessions', function (Blueprint $table) {
            $table->increments('id');
            
            // Panel ID
            $table->unsignedInteger('panel_id');
            $table->foreign('panel_id')->references('id')->on('panels')->onDelete('cascade');
            $table->dropForeign(['panel_id']);

            $table->integer('session_number');
            $table->string('key', 8)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('panel_sessions');
    }
}
