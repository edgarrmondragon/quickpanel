@extends('layouts.master')

@section('title', 'Páneles')

@section('content')

<div class="container">
  <div class="row">
    <div class="col s12">
    @unless (count($panels))
      <p>There are no panels.</p>
    @else
      <ul class="collapsible">
        @foreach ($panels as $panel)
          <li>
            <div class="collapsible-header">{{ $panel->name }}</div>
            <div class="collapsible-body">
              <table>
                <caption>Sesiones</caption>
                <thead>
                  <tr>
                    <th>Número de sesión</th>
                    <th>Llave de sesión</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($panel->panelSessions()->get() as $panelSession)
                  <tr>
                    <td>{{ $panelSession->session_number }}</td>
                    <td>{{ $panelSession->key }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </li>
        @endforeach
      </ul>
      <hr>
    @endunless
    </div>
  </div>
</div>

@endsection