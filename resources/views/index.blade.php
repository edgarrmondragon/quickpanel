@extends('layouts.master')

@section('title', 'Inicio')

@section('sidebar')
  @parent

  <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
  
  {{-- <a href=""><img class="responsive-img" style="width: 250px;" src="img/quickpanel.png" /></a> --}}
  <header>
    <a href="" class="quickpanel-brand big-header">QuickPanel</a>
  </header>

    {{-- <h5 class="indigo-text"><strong>Accede</strong></h5> --}}

  {{-- Dummy rows for spacing  --}}
  <div class="row"></div>
  <div class="row"></div>
  {{-- <div class="row"></div> --}}

  <div class="row">
    <div class="z-depth-1 grey lighten-4 row hoverable login-box">
      
      <ul class="tabs tabs-fixed-width">
        <li class="tab"><a href="#panelist-login">Panelista</a></li>
        <li class="tab"><a href="#researcher-login">Investigador</a></li>
      </ul>

      <div id="panelist-login">
        @include('login-panelist')
      </div>
      <div id="researcher-login">
        @include('login-researcher')
      </div>
    
    </div>
  </div>
  
@endsection
