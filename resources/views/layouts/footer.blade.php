<footer class="page-footer">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <strong class="quickpanel-brand"">QuickPanel</strong> ofrece servicios de recolección de datos en línea para <strong>páneles sensoriales</strong>, <strong><em>focus groups</em></strong>, y <strong>pruebas con consumidores.</strong>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
    © 2018 Creado por Edgar Ramírez Mondragón
    <a class="grey-text text-lighten-4 right" href="info">Info</a>
    </div>
  </div>
</footer>
