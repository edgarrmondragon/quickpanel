<div id="login-researcher" class="row">
  <form class="login-form" action="">
    @csrf
    <div class="row">
      <div class="input-field col s12">
        <i class="material-icons prefix">person_outline</i>
        <input id="username" type="text" autocomplete="username" name="username">
        <label for="username" class="center-align">Usuario</label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s12">
        <i class="material-icons prefix">lock_outline</i>
        <input id="password" type="password" autocomplete="current-password" name="password">
        <label for="password">Contraseña</label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s12 m6 l6 offset-m3 offset-l3">
        <button class="btn waves-effect waves-light col s12" value="Client" type="submit" name="submit" >Entrar</button>
      </div>
    </div>
    {{-- <div class="row">
      <div class="input-field col s6 m6 l6">
        <p class="margin medium-small"><a href="page-register.html">¡Regístrate!</a></p>
      </div>
      <div class="input-field col s6 m6 l6">
        <p class="margin right-align medium-small"><a href="page-forgot-password.html">Olvidé mi contraseña</a></p>
      </div>
    </div> --}}
  </form>
</div>