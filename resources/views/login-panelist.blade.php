<div id="login-panelist" class="row">
  <form class="login-form" action="">
    @csrf
    <div class="row">
      <div class="input-field col s12">
        <i class="material-icons prefix">vpn_key</i>
        <input id="session-key" type="text" maxlength="8" name="session-key">
        <label for="session-code" class="center-align">Llave de sesión</label>
      </div>
    </div>
    <div class="row">
      <div class="input-field col s12 m6 l6 offset-m3 offset-l3">
        <button class="btn waves-effect waves-light col s12" value="Panelist" type="submit" name="submit" >Entrar</button>
      </div>
    </div>
  </form>
</div>